﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float dashForce = 400;
	public float DASH_COOLDOWN = 0.2f;
	public float PUSH_COOLDOWN = 0.8f;
	public Transform dashPrefab;

	public bool disableInput = false;

	float pushCooldown = 0.0f;
	float dashCooldownU = 0.0f;
	float dashCooldownD = 0.0f;
	float dashCooldownR = 0.0f;
	float dashCooldownL = 0.0f;

	void Dash(float angle) {
		Transform dashEffect = GameObject.Instantiate(dashPrefab, transform.position, Quaternion.Euler(0,0,angle)) as Transform;
		dashEffect.parent = transform;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}

		pushCooldown -= Time.deltaTime;
		dashCooldownU -= Time.deltaTime;
		dashCooldownD -= Time.deltaTime;
		dashCooldownR -= Time.deltaTime;
		dashCooldownL -= Time.deltaTime;
		
		if(!disableInput) {
			if(pushCooldown <= 0 && Input.GetKeyDown(KeyCode.Space)) {
				pushCooldown = PUSH_COOLDOWN;
				GetComponent<Pusher>().Push();
			}

			if(dashCooldownU <= 0 && Input.GetKeyDown(KeyCode.UpArrow)) {
				dashCooldownU = DASH_COOLDOWN;
				GetComponent<Rigidbody>().AddForce(0.7f * dashForce * Vector3.up * GetComponent<Rigidbody>().mass);
				Dash(90);
			}
			if(dashCooldownD <= 0 && Input.GetKeyDown(KeyCode.DownArrow)) {
				dashCooldownD = DASH_COOLDOWN;
				GetComponent<Rigidbody>().AddForce(dashForce * Vector3.down * GetComponent<Rigidbody>().mass);
				Dash(-90);
			}
			if(dashCooldownR <= 0 && Input.GetKeyDown(KeyCode.RightArrow)) {
				dashCooldownR = DASH_COOLDOWN;
				GetComponent<Rigidbody>().AddForce(dashForce * Vector3.right * GetComponent<Rigidbody>().mass);
				Dash(0);
			}
			if(dashCooldownL <= 0 && Input.GetKeyDown(KeyCode.LeftArrow)) {
				dashCooldownL = DASH_COOLDOWN;
				GetComponent<Rigidbody>().AddForce(dashForce * Vector3.left * GetComponent<Rigidbody>().mass);
				Dash(180);
			}
		}
	}
}
