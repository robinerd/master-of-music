﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveController : MonoBehaviour {

	public Music music;
	public Color acquiredColor;
	public Color acquiredColorBG;

	Image waveImg;
	Color originalColor;
	Player player;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindObjectOfType<Player>();
		waveImg = transform.FindChild("Wave").GetComponent<Image>();

		originalColor = waveImg.color;
		waveImg.color = Color.clear;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(music.GetComponentInParent<Enemy>() == null) {
			//Player acquired this sound
			originalColor = acquiredColor;
			GetComponent<Image>().color = acquiredColorBG;
		}

		float alpha = Mathf.Clamp01((14 - (music.transform.position - player.transform.position).magnitude) / 14);
		alpha = alpha * alpha;
		waveImg.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
	}
}
