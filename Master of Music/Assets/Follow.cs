﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

	public GameObject target;
	Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float factor = 1.2f * Time.deltaTime;
		Vector3 targetPos = target.transform.position + offset;
		Vector3 newPos = transform.position;

		//Attraction force increasing by distance
		newPos = Vector3.Lerp(newPos, targetPos, factor);
		newPos = Vector3.Lerp(newPos, targetPos, factor);
		newPos = Vector3.Lerp(newPos, targetPos, factor);

		transform.position = newPos;
	}
}
