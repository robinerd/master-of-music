﻿using UnityEngine;
using System.Collections;

public class ContinueButton : MonoBehaviour {

	public Transform nextLevelPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return)) {
			GameObject.Instantiate(nextLevelPrefab);
			enabled = false;
		}
	}
}
