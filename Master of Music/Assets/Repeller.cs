﻿using UnityEngine;
using System.Collections;

public class Repeller : MonoBehaviour {

	public float force = 4000;
	public float radius = 2f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		BoxCollider[] walls = GameObject.FindObjectsOfType<BoxCollider>();
		foreach(BoxCollider wall in walls) {
			float direction = Mathf.Sign(Vector3.Dot(
				wall.transform.up,
				(transform.position - wall.transform.position)));
			float distance = (transform.position - wall.transform.position).magnitude;

			float overlapFactor = Mathf.Clamp01((radius - Mathf.Abs(distance)) / radius);

			if(overlapFactor > 0) {
				//Colliding!
				float repellation = direction * overlapFactor * force * Time.deltaTime;
				GetComponent<Rigidbody>().AddForce(repellation * wall.transform.up);
				wall.GetComponent<Rigidbody>().AddForce(-3 * repellation * wall.transform.up);
			}
		}
	}
}
