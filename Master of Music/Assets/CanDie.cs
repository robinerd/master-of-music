﻿using UnityEngine;
using System.Collections;

public class CanDie : MonoBehaviour {

	public Transform spawnPrefab = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < 0) {
			Die();
		}
	}

	void Die()
	{
		if(spawnPrefab) {
			Instantiate(spawnPrefab, transform.position, Quaternion.identity);
		}

		if(GetComponent<Enemy>() != null) {
			GetComponentInChildren<Music>().Acquire();
			ScoreManager.Instance.kills++;
		}

		Destroy(gameObject);

		if(GetComponent<Player>() != null) {
			//TODO: show game over or something.
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
