﻿using UnityEngine;
using System.Collections;

public class Pusher : MonoBehaviour {

	public float force = 12000;
	public float radius = 7;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Push()
	{
		Transform pushEffect = transform.FindChild("Pusheffect");
		pushEffect.GetComponent<ParticleSystem>().Play();
		pushEffect.GetComponent<AudioSource>().Stop();
		pushEffect.GetComponent<AudioSource>().Play();

		GameObject[] pushables = GameObject.FindGameObjectsWithTag("pushable");
		
		foreach(GameObject pushable in pushables) {
			pushable.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, radius);
		}
	}
}
