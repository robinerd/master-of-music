﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int currentLevel = 1;
	public static ScoreManager Instance;

	public Transform winPrefab;

	int killsToWin;
	int _kills = 0;

	public int kills {
		get {
			return _kills;
		}
		set {
			_kills = value;
			Debug.Log("Kills: "+_kills);
			if(_kills >= killsToWin) {
				Win();
			}
		}
	}

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		killsToWin = GameObject.FindObjectsOfType<Enemy>().Length;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Win() {
		GameObject.FindObjectOfType<Player>().disableInput = true;
		currentLevel++;
		GameObject.Instantiate(winPrefab);
	}
}
