﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wobbler : MonoBehaviour {
	public float springCoef = 1.0f;
	public float range = 2.0f;
	//public float offset = 0.0f;
	//public float prevOffset = 0.0f;
	public Vector3 originalPos;

	List<Wobbler> adjacentWobblers;

	public float offset {
		get {
			return Vector3.Dot(transform.position - originalPos, transform.up);
		}
	}


	// Use this for initialization
	void Start () {
		originalPos = transform.position;

		adjacentWobblers = new List<Wobbler>(4);
		
		Wobbler[] wobblers = GameObject.FindObjectsOfType<Wobbler>();
		foreach(Wobbler wobbler in wobblers) {
			if((originalPos - wobbler.originalPos).sqrMagnitude < range*range)
			{
				//Adjacent!
				adjacentWobblers.Add(wobbler);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		updateWobble();
		updatePos();
	}

	void updateWobble() {
		//float oldOffset = offset;
		float force = 0;

		foreach(Wobbler wobbler in adjacentWobblers) {
			force += (wobbler.offset - offset);
		}

		GetComponent<Rigidbody>().AddForce(transform.up * force * Time.deltaTime * 12000f);

		//offset += (offset - prevOffset) + force * springCoef * Time.deltaTime;
		//offset *= 1.0f - (0.6f*Time.deltaTime);
		//prevOffset = oldOffset;
	}

	void updatePos() {
		transform.position = originalPos + transform.up * offset;
	}
}
