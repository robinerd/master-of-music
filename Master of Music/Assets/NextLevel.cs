﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NextLevel : MonoBehaviour {

	public static int NUMBER_OF_LEVELS = 6;
	float changeLevelTimer = 1.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		changeLevelTimer -= Time.deltaTime;

		Color col = GameObject.FindGameObjectWithTag("Blocker").GetComponent<GUITexture>().color;
		col.a = (1.0f - changeLevelTimer) / 1.0f;
		GameObject.FindGameObjectWithTag("Blocker").GetComponent<GUITexture>().color = col;

		if(changeLevelTimer < 0) {
			if(ScoreManager.currentLevel <= NUMBER_OF_LEVELS) {
				Application.LoadLevel("level"+ScoreManager.currentLevel);
			}
			else {
				Application.Quit();
			}
		}
	}
}
