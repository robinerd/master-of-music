﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	public float originalVolume;

	bool acquired = false;
	float targetVolume = 0;

	// Use this for initialization
	void Awake () {
		originalVolume = GetComponent<AudioSource>().volume;
		targetVolume = originalVolume;
		GetComponent<AudioSource>().volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<AudioSource>().volume = Mathf.Lerp(GetComponent<AudioSource>().volume, targetVolume, 0.8f * Time.deltaTime);

		if(acquired) {
			transform.position = Vector3.Lerp(transform.position, transform.parent.position, 1.3f * Time.deltaTime);
		}
	}

	public void Acquire() {
		acquired = true;
		Transform playerSparkles = GameObject.FindObjectOfType<Player>().transform.FindChild("Sparkles");
		Transform enemySparkles = transform.FindChild("Sparkles");
		enemySparkles.GetComponent<ParticleSystem>().startColor = playerSparkles.GetComponent<ParticleSystem>().startColor;
		enemySparkles.GetComponent<ParticleSystem>().startSpeed += 1.0f + ScoreManager.Instance.kills * 1.5f;
		enemySparkles.GetComponent<ParticleSystem>().startLifetime += 0.1f + ScoreManager.Instance.kills * 0.08f;
		enemySparkles.GetComponent<ParticleSystem>().emissionRate += 20.0f;

		transform.parent = GameObject.FindObjectOfType<Player>().transform;
		targetVolume = GetComponent<AudioSource>().volume * 1.2f;
		GetComponent<AudioSource>().volume = 0;
	}
}
