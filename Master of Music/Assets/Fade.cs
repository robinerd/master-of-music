﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	[System.Serializable]
	public struct FadePoint {
		public Color color;
		public float timeToNext;
	}

	bool loop = false;
	bool autoDestroy = true;

	public FadePoint[] points;

	int currentIndex;
	float timer = 0.0f;

	FadePoint current {
		get { return points[currentIndex]; }
	}

	FadePoint next {
		get {
			int index = currentIndex + 1;
			if(index >= points.Length) {
				index = loop ? 0 : currentIndex;
			}
			return points[index];
		}
	}

	// Use this for initialization
	void Awake () {
		GetComponent<MeshRenderer>().material.SetColor("_TintColor", points[0].color);
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer >= current.timeToNext) {
			timer = 0.0f;
			currentIndex++;
			if(currentIndex >= points.Length) {
				if(autoDestroy) {
					Destroy(gameObject);
					enabled = false;
					return;
				}
				else if(loop) {
					currentIndex = 0;
				}
				else {
					GetComponent<MeshRenderer>().material.SetColor("_TintColor", points[points.Length - 1].color);
					enabled = false;
					return;
				}
			}
		}

		GetComponent<MeshRenderer>().material.SetColor("_TintColor", Color.Lerp(current.color, next.color, timer / current.timeToNext));
	}
}
